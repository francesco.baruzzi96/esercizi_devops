import mysql.connector
import os
import time

time.sleep(5)

mydb = mysql.connector.connect(host=os.environ["MYSQL_SERVER"], 
user=os.environ["MYSQL_USER"],
password=os.environ["MYSQL_PASSWORD"])

cursor = mydb.cursor()
databases = ("show databases")
cursor.execute(databases)
for (db) in cursor:
    print (db[0])