#pragma once

#ifdef _WIN32
  #define funzioni_EXPORT __declspec(dllexport)
#else
  #define funzioni_EXPORT
#endif

funzioni_EXPORT void funzioni();

funzioni_EXPORT int somma(int a, int b);