#include <rapidcheck.h>

int somma (int a, int b){
    return a+b;
}

int somma_errata (int a, int b){
    return a+b-a+b;
}

int main(int argc, char** argv){
    rc::check("test",[](int a, int b){
        int s1=somma(a,b);
        int s2=somma(b,a);
        RC_ASSERT(s1==s2);
    });
    rc::check("test",[](int a, int b){
        int s1=somma_errata(a,b);
        int s2=somma_errata(b,a);
        RC_ASSERT(s1==s2);
    });
return 0;
}


