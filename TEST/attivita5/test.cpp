#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "operatori.h"
/*
TEST_CASE("incremento"){
    //arrange 
    int a;
    a=5;
    SECTION("TEST_1"){
        inc(&a);
    }
    REQUIRE(a==6);
}


TEST_CASE("decremento"){
    //arrange 
    int a;
    a=5;
    SECTION("TEST_1"){
        dec(&a);
    }
    REQUIRE(a==4);
}

TEST_CASE("sezioni"){
    //arrange 
    int a;
    a=5;
    SECTION("dec"){
        dec(&a);
        REQUIRE(a==4);
    }

    SECTION("inc"){
    inc(&a);
    REQUIRE(a==6);
        SECTION("inc_inc"){
        inc(&a);
        REQUIRE(a==7);
        }
        SECTION("inc_dec"){
        dec(&a);
        REQUIRE(a==5);
        }
    }
}**/

TEST_CASE("generatori"){
    //arrange 
    auto a = GENERATE(range(1,5));
    auto res = a+1;
    inc(&a);
    REQUIRE(a==res);
}


TEST_CASE("generatori_1"){
    //arrange 
    auto a = GENERATE(take(100,random(-100,100)));
    auto res = a+1;
    inc(&a);
    REQUIRE(a==res);
}
