#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "../src/main.h"

TEST_CASE("PROVA_1"){
    //arrange 
    int i = 5;
    int s = 3;
    // act 
    int o = somma(i,s);
    REQUIRE(o==3);
}

TEST_CASE("PROVA_2"){
    //arrange 
    int i = 5;
    int s = 3;
    // act 
    int o = somma(i,s);
    REQUIRE(o==8);
}