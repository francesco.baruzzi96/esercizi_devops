#include "funzioni_test.h"
#define CATCH_CONFIG_MAIN
#include "catch.hpp"





TEST_CASE("PROVA_2", "[ui]"){
    //arrange 
    int i = 5;
    int s = 3;
    // act 
    int o = somma(i,s);
    REQUIRE(o==8);
}


TEST_CASE("PROVA_1", "[parser]"){
    //arrange 
    int i = 5;
    int s = 4;
    // act 
    int o = somma(i,s);
    REQUIRE(o==9);
}