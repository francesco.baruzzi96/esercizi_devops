########## MACROS ###########################################################################
#############################################################################################

# Requires CMake > 3.15
if(${CMAKE_VERSION} VERSION_LESS "3.15")
    message(FATAL_ERROR "The 'CMakeDeps' generator only works with CMake >= 3.15")
endif()

if(funzioni_test_FIND_QUIETLY)
    set(funzioni_test_MESSAGE_MODE VERBOSE)
else()
    set(funzioni_test_MESSAGE_MODE STATUS)
endif()

include(${CMAKE_CURRENT_LIST_DIR}/cmakedeps_macros.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/funzioni_testTargets.cmake)
include(CMakeFindDependencyMacro)

check_build_type_defined()

foreach(_DEPENDENCY ${funzioni_test_FIND_DEPENDENCY_NAMES} )
    # Check that we have not already called a find_package with the transitive dependency
    if(NOT ${_DEPENDENCY}_FOUND)
        find_dependency(${_DEPENDENCY} REQUIRED ${${_DEPENDENCY}_FIND_MODE})
    endif()
endforeach()

set(funzioni_test_VERSION_STRING "0.1")
set(funzioni_test_INCLUDE_DIRS ${funzioni_test_INCLUDE_DIRS_RELEASE} )
set(funzioni_test_INCLUDE_DIR ${funzioni_test_INCLUDE_DIRS_RELEASE} )
set(funzioni_test_LIBRARIES ${funzioni_test_LIBRARIES_RELEASE} )
set(funzioni_test_DEFINITIONS ${funzioni_test_DEFINITIONS_RELEASE} )

# Only the first installed configuration is included to avoid the collision
foreach(_BUILD_MODULE ${funzioni_test_BUILD_MODULES_PATHS_RELEASE} )
    message(${funzioni_test_MESSAGE_MODE} "Conan: Including build module from '${_BUILD_MODULE}'")
    include(${_BUILD_MODULE})
endforeach()


