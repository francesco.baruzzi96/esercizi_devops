# Load the debug and release variables
get_filename_component(_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
file(GLOB DATA_FILES "${_DIR}/funzioni_test-*-data.cmake")

foreach(f ${DATA_FILES})
    include(${f})
endforeach()

# Create the targets for all the components
foreach(_COMPONENT ${funzioni_test_COMPONENT_NAMES} )
    if(NOT TARGET ${_COMPONENT})
        add_library(${_COMPONENT} INTERFACE IMPORTED)
        message(${funzioni_test_MESSAGE_MODE} "Conan: Component target declared '${_COMPONENT}'")
    endif()
endforeach()

if(NOT TARGET funzioni_test::funzioni_test)
    add_library(funzioni_test::funzioni_test INTERFACE IMPORTED)
    message(${funzioni_test_MESSAGE_MODE} "Conan: Target declared 'funzioni_test::funzioni_test'")
endif()
# Load the debug and release library finders
get_filename_component(_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
file(GLOB CONFIG_FILES "${_DIR}/funzioni_test-Target-*.cmake")

foreach(f ${CONFIG_FILES})
    include(${f})
endforeach()