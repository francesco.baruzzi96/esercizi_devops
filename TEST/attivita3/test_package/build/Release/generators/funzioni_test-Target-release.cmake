# Avoid multiple calls to find_package to append duplicated properties to the targets
include_guard()########### VARIABLES #######################################################################
#############################################################################################
set(funzioni_test_FRAMEWORKS_FOUND_RELEASE "") # Will be filled later
conan_find_apple_frameworks(funzioni_test_FRAMEWORKS_FOUND_RELEASE "${funzioni_test_FRAMEWORKS_RELEASE}" "${funzioni_test_FRAMEWORK_DIRS_RELEASE}")

set(funzioni_test_LIBRARIES_TARGETS "") # Will be filled later


######## Create an interface target to contain all the dependencies (frameworks, system and conan deps)
if(NOT TARGET funzioni_test_DEPS_TARGET)
    add_library(funzioni_test_DEPS_TARGET INTERFACE IMPORTED)
endif()

set_property(TARGET funzioni_test_DEPS_TARGET
             PROPERTY INTERFACE_LINK_LIBRARIES
             $<$<CONFIG:Release>:${funzioni_test_FRAMEWORKS_FOUND_RELEASE}>
             $<$<CONFIG:Release>:${funzioni_test_SYSTEM_LIBS_RELEASE}>
             $<$<CONFIG:Release>:>
             APPEND)

####### Find the libraries declared in cpp_info.libs, create an IMPORTED target for each one and link the
####### funzioni_test_DEPS_TARGET to all of them
conan_package_library_targets("${funzioni_test_LIBS_RELEASE}"    # libraries
                              "${funzioni_test_LIB_DIRS_RELEASE}" # package_libdir
                              funzioni_test_DEPS_TARGET
                              funzioni_test_LIBRARIES_TARGETS  # out_libraries_targets
                              "_RELEASE"
                              "funzioni_test")    # package_name

# FIXME: What is the result of this for multi-config? All configs adding themselves to path?
set(CMAKE_MODULE_PATH ${funzioni_test_BUILD_DIRS_RELEASE} ${CMAKE_MODULE_PATH})

########## GLOBAL TARGET PROPERTIES Release ########################################
    set_property(TARGET funzioni_test::funzioni_test
                 PROPERTY INTERFACE_LINK_LIBRARIES
                 $<$<CONFIG:Release>:${funzioni_test_OBJECTS_RELEASE}>
                 $<$<CONFIG:Release>:${funzioni_test_LIBRARIES_TARGETS}>
                 APPEND)

    if("${funzioni_test_LIBS_RELEASE}" STREQUAL "")
        # If the package is not declaring any "cpp_info.libs" the package deps, system libs,
        # frameworks etc are not linked to the imported targets and we need to do it to the
        # global target
        set_property(TARGET funzioni_test::funzioni_test
                     PROPERTY INTERFACE_LINK_LIBRARIES
                     funzioni_test_DEPS_TARGET
                     APPEND)
    endif()

    set_property(TARGET funzioni_test::funzioni_test
                 PROPERTY INTERFACE_LINK_OPTIONS
                 $<$<CONFIG:Release>:${funzioni_test_LINKER_FLAGS_RELEASE}> APPEND)
    set_property(TARGET funzioni_test::funzioni_test
                 PROPERTY INTERFACE_INCLUDE_DIRECTORIES
                 $<$<CONFIG:Release>:${funzioni_test_INCLUDE_DIRS_RELEASE}> APPEND)
    set_property(TARGET funzioni_test::funzioni_test
                 PROPERTY INTERFACE_COMPILE_DEFINITIONS
                 $<$<CONFIG:Release>:${funzioni_test_COMPILE_DEFINITIONS_RELEASE}> APPEND)
    set_property(TARGET funzioni_test::funzioni_test
                 PROPERTY INTERFACE_COMPILE_OPTIONS
                 $<$<CONFIG:Release>:${funzioni_test_COMPILE_OPTIONS_RELEASE}> APPEND)

########## For the modules (FindXXX)
set(funzioni_test_LIBRARIES_RELEASE funzioni_test::funzioni_test)
