########### AGGREGATED COMPONENTS AND DEPENDENCIES FOR THE MULTI CONFIG #####################
#############################################################################################

set(funzioni_test_COMPONENT_NAMES "")
set(funzioni_test_FIND_DEPENDENCY_NAMES "")

########### VARIABLES #######################################################################
#############################################################################################
set(funzioni_test_PACKAGE_FOLDER_RELEASE "/home/ute/.conan/data/funzioni_test/0.1/_/_/package/6557f18ca99c0b6a233f43db00e30efaa525e27e")
set(funzioni_test_BUILD_MODULES_PATHS_RELEASE )


set(funzioni_test_INCLUDE_DIRS_RELEASE "${funzioni_test_PACKAGE_FOLDER_RELEASE}/include")
set(funzioni_test_RES_DIRS_RELEASE )
set(funzioni_test_DEFINITIONS_RELEASE )
set(funzioni_test_SHARED_LINK_FLAGS_RELEASE )
set(funzioni_test_EXE_LINK_FLAGS_RELEASE )
set(funzioni_test_OBJECTS_RELEASE )
set(funzioni_test_COMPILE_DEFINITIONS_RELEASE )
set(funzioni_test_COMPILE_OPTIONS_C_RELEASE )
set(funzioni_test_COMPILE_OPTIONS_CXX_RELEASE )
set(funzioni_test_LIB_DIRS_RELEASE "${funzioni_test_PACKAGE_FOLDER_RELEASE}/lib")
set(funzioni_test_LIBS_RELEASE funzioni_test)
set(funzioni_test_SYSTEM_LIBS_RELEASE )
set(funzioni_test_FRAMEWORK_DIRS_RELEASE )
set(funzioni_test_FRAMEWORKS_RELEASE )
set(funzioni_test_BUILD_DIRS_RELEASE "${funzioni_test_PACKAGE_FOLDER_RELEASE}/")

# COMPOUND VARIABLES
set(funzioni_test_COMPILE_OPTIONS_RELEASE
    "$<$<COMPILE_LANGUAGE:CXX>:${funzioni_test_COMPILE_OPTIONS_CXX_RELEASE}>"
    "$<$<COMPILE_LANGUAGE:C>:${funzioni_test_COMPILE_OPTIONS_C_RELEASE}>")
set(funzioni_test_LINKER_FLAGS_RELEASE
    "$<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:${funzioni_test_SHARED_LINK_FLAGS_RELEASE}>"
    "$<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:${funzioni_test_SHARED_LINK_FLAGS_RELEASE}>"
    "$<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:${funzioni_test_EXE_LINK_FLAGS_RELEASE}>")


set(funzioni_test_COMPONENTS_RELEASE )