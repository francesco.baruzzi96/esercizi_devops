#define CATCH_CONFIG_MAIN
#include "catch.hpp"

TEST_CASE("PROVA_1"){
    //arrange 
    int i = 5;
    // act 
    int o = i + 1;
    REQUIRE(o==i);
}

TEST_CASE("PROVA_2"){
    //arrange 
    int i = 5;
    // act 
    int o = i;
    REQUIRE(o==i);
}